#!/usr/bin/env python
#
# Nagios Plugin to test a DICOM server
# Paul Stevens - Jan 2006
# R. Hartley-Davies - April 2011
#
# NB no validation of arguments as yet
#

import os, subprocess
import sys, getopt

# default caller, normally we don't need to change this
local_aet = 'NAGIOS_CHECK'

# nagios sys exit codes
OK,WARNING,CRITICAL,UNKNOWN = 0,1,2,3


def usage():
    print "usage: check_dicom [-a <calling aet>] <server aet> <server host> <server port no.>"

try:
    opts, args = getopt.getopt(sys.argv[1:], 'ha:', ['help',])
except getopt.GetoptError:
    usage()
    sys.exit(UNKNOWN)

for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(OK)
    if o == '-a':
        local_aet = a

if len(args)!=3:
    if len(args)<3:
        print 'Too few arguments'
    else:
        print 'Too many arguments'
    usage()
    sys.exit(UNKNOWN)

# use dcm4che echo command
ping_cmd = ('/usr/local/dcm4che2/bin/dcmecho', '-L', local_aet, '%s@%s:%s' % (args[0], args[1], args[2]))
p = subprocess.Popen(ping_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
p.stdout.readlines()
err_text = p.stderr.readlines()
status = p.wait()
if status == 0:
    print 'DICOM OK - DICOM echo accepted'
    sys.exit(OK)
else:
    line = err_text[-1]
    print 'DICOM Critical - ', line[line.rfind(':') + 1:-1]
    sys.exit(CRITICAL)


