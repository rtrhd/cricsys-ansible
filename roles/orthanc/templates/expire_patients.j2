#!/usr/bin/env python3

"""
Delete DICOM files from Orthanc server via the REST API.

Example usage:
    expire_patients -d 90 -x 'MR, US' -m '^CRIS' -f -P 80

This deletes patients where all their studies are:
- older than 90 days
- excluding MRI and ultrasound modalities
- with PatientIDs starting with 'CRIS'
- without prompting the user to confirm deletion
- using port 80

Authors:
    Claire Doody (claire.doody@nhs.net)
    Jonathon Delve (jonathon.delve@nbt.nhs.uk)
    R. Hartley-Davies (R.Hartley-Davies@physics.org)
"""
import sys
import logging
from logging import WARN, INFO, DEBUG
from datetime import datetime, timedelta
from functools import partial
from itertools import chain
from argparse import ArgumentParser, ArgumentTypeError, RawDescriptionHelpFormatter
from getpass import getpass
from re import search
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import requests
from requests.exceptions import ConnectionError, HTTPError

# Default values
HOST = 'localhost'
PORT = '{{("443" if rproxy_https else "80") if apache2_rproxy else http_port}}'
USER = '{{admin_user}}'
PASSWORD = '{{admin_passwd}}'
PREFIX = '{{rproxy_path if apache2_rproxy else ""}}'
SECURE = {{rproxy_https}}
JSON = {'Accept': 'application/json'}
LEVELS = ['patients', 'studies', 'series', 'instances']

EXAMPLE="""\
Example:

expire_patients -d 90 -x 'MR, US' -m '^CRIS' -f -P 80

This deletes patients where all their studies are:
- older than 90 days
- excluding MRI and ultrasound modalities
- with PatientIDs starting with 'CRIS'
- without prompting the user to confirm deletion
- using port 80
"""

VERSION = '0.3.0'


def _all_items_at_level(level):
    """Get list of dbids at given level."""
    assert level in LEVELS
    response = requests.get(
        '/'.join([ENDPOINT, level]),
        headers=JSON,
        verify=False
    )
    response.raise_for_status()
    return response.json()


all_patients = partial(_all_items_at_level, "patients")


def _item_record_at_level(level, dbid):
    """Get info structure of db object at given level as python dict."""
    assert level in LEVELS
    response = requests.get(
        '/'.join([ENDPOINT, level, dbid]),
        headers=JSON,
        verify=False
    )
    response.raise_for_status()
    return response.json()


patient_record = partial(_item_record_at_level, "patients")
study_record = partial(_item_record_at_level, "studies")
series_record = partial(_item_record_at_level, "series")


def studies_for_patient(dbid):
    """Database ids of studies belonging to given patient."""
    return patient_record(dbid)['Studies']


def series_for_study(dbid):
    """Database ids of series belonging to given study."""
    return study_record(dbid)['Series']


def study_date(dbid):
    """Study date from study database id."""
    if study_record(dbid)['MainDicomTags']['StudyDate'] == '':
        logging.info('Study %s has empty date field, skipping...', study_record(dbid)['ID'])
        return datetime.now()
    else:
        return datetime.strptime(
            study_record(dbid)['MainDicomTags']['StudyDate'],
            '%Y%m%d'
        )


def patient_latest_study_date(dbid):
    """Most recent study date from patient database id."""
    return max(study_date(stuid) for stuid in studies_for_patient(dbid))


def series_modality(dbid):
    """Modality of given series."""
    return series_record(dbid)['MainDicomTags']['Modality']


def study_modalities(study_dbid):
    """Set of modalities of given study."""
    return set(
        series_modality(series_dbid)
        for series_dbid in series_for_study(study_dbid)
    )


def patient_modalities(pat_dbid):
    """Set of modalities of given patient."""
    return set.union(*[
        study_modalities(study_dbid)
        for study_dbid in studies_for_patient(pat_dbid)
    ])


def delete_at_level(level, dbid):
    """Delete object from database at the given level."""
    assert level in ['patients', 'study', 'series']
    requests.delete(
        '/'.join([ENDPOINT, level, dbid], verify=False)
    ).raise_for_status()


def delete_patient(dbid):
    """Delete patient object from database."""
    patid = patient_record(dbid)['MainDicomTags']['PatientID']
    logging.info('Deleting patient %s (%s)', dbid, patid)
    delete_at_level('patients', dbid)


def parse_args():
    """Parse command line arguments."""
    def modalities(arg):
        """Type handler for modality/modalities - comma separated lists."""
        try:
            return list(set(s.strip() for s in arg.split(',')))
        except ValueError:
            raise ArgumentTypeError(
                "'%s' is not a valid modality or list" % arg
            )

    parser = ArgumentParser(
        description="Delete Patients on Orthanc by their studies' age, modality and PatientID.",
        epilog=EXAMPLE,
        formatter_class=RawDescriptionHelpFormatter
    )

    parser.add_argument(
        '-d', '--days', action='store', type=int, default={{expiry_time}},
        help="age in days from the present of the newest study date to consider (default: %(default)s)"
    )

    parser.add_argument(
        '-i', '--include', action='append', type=modalities, default=[],
        help='modalities to include, comma separated list'
    )

    parser.add_argument(
        '-x', '--exclude', action='append', type=modalities, default=[],
        help='modalities to exclude, comma separated list'
    )

    parser.add_argument(
        '-m', '--match', action='store', type=str, default=None,
        help='regex pattern to filter patient IDs: matches are included'
    )

    parser.add_argument(
        '-f', '--force', action='store_true',
        help='do not ask before deleting patients'
    )

    parser.add_argument(
        '-u', '--user', default=USER,
        help='name of user on orthanc server (default: %(default)s)'
    )

    parser.add_argument(
        '-p', '--password', nargs="?", const=None, default=PASSWORD,
        help='password of user on orthanc server'
    )

    parser.add_argument(
        '-H', '--host', default=HOST,
        help='hostname of orthanc server (default: %(default)s)'
    )

    parser.add_argument(
        '-P', '--port', default=PORT,
        help='tcp/ip port of orthanc server (default: %(default)s)'
    )

    parser.add_argument(
        '-r', '--prefix', default=PREFIX,
        help='rest end point on server when proxied (default: %(default)s)'
    )

    parser.add_argument(
        '-s', '--secure', default=SECURE,
        help='Use tls when proxied (default: %(default)s)'
    )

    parser.add_argument(
        '-v', '--verbose', action='count', default=0,
        help="verbose logging"
    )

    parser.add_argument(
        '-V', '--version', action='version',
        version='%(prog)s {}'.format(VERSION)
    )

    args = parser.parse_args()

    # Flatten and uniqify modality lists
    args.include = list(set(chain.from_iterable(args.include)))
    args.exclude = list(set(chain.from_iterable(args.exclude)))

    if args.password is None:
        args.password = getpass()
    return args


def ask_user(question, default=None, nattempts=5):
    """Ask a yes/no question via raw_input()/input() and return as a bool."""
    assert default in [True, False, None]
    assert nattempts >= 1

    prompt = "Y/n" if default is True else "y/N" if default is False else "y/n"
    input_fn = raw_input if sys.version_info < (3,) else input
    attempts = 0
    while attempts < nattempts:
        choice = input_fn(' '.join([question, '[%s]: ' % prompt])).lower()
        if choice and not choice.startswith('q'):
            if choice.lower().startswith('y'):
                return True
            elif choice.lower().startswith('n'):
                return False
            else:
                sys.stdout.write("Type either 'yes' or 'no'\n")
                attempts += 1
        elif default is not None:
            return default
        elif choice.startswith('q') or attempts >= nattempts:
            return False
        else:
            attempts += 1
    return False


def expire(days, include, exclude, match, force, user, password, host, port, prefix, secure):
    """Expire Patients based on study age and modality."""

    global ENDPOINT
    protocol = 'https' if secure else 'http'
    if prefix:
        ENDPOINT = "%s://%s:%s@%s:%s/%s" % (protocol, user, password, host, port, prefix)
    else:
        ENDPOINT = "%s://%s:%s@%s:%s" % (protocol, user, password, host, port)

    patient_dbids = all_patients()

    logging.info('Limiting to patients with studies older than %d days', days)
    patient_dbids = [
        dbid for dbid in patient_dbids
        if (datetime.now() - patient_latest_study_date(dbid)) > timedelta(days)
    ]

    if include:
        logging.info(
            'Limiting to patients with modalities: %s', ', '.join(include)
        )
        patient_dbids = [
            dbid for dbid in patient_dbids
            if patient_modalities(dbid).issubset(include)
        ]

    if exclude:
        logging.info(
            'Limiting to patients without modalities: %s', ', '.join(exclude)
        )
        patient_dbids = [
            dbid for dbid in patient_dbids
            if patient_modalities(dbid).isdisjoint(exclude)
        ]

    if match:
        logging.info(
            'Limiting to patients with IDs matching: %r', match
        )
        patient_dbids = [
            dbid for dbid in patient_dbids
            if search(match, patient_record(dbid)['MainDicomTags']['PatientID'])
        ]

    logging.info('Left with %d patients to remove.', len(patient_dbids))

    for dbid in patient_dbids:
        patid = patient_record(dbid)['MainDicomTags']['PatientID']
        if force or ask_user('Delete Patient %s' % patid, default=True):
            logging.info('Deleting patient %s', patid)
            delete_patient(dbid)
        else:
            logging.info('Ignoring patient %s', patid)

    logging.info('Finished.')


if __name__ == "__main__":
    from signal import signal, SIGINT

    def _handler(sig, frame):
        print(file=sys.stderr)
        sys.exit(1)
    signal(SIGINT, _handler)

    # vars() converts Namespace -> dict
    args = vars(parse_args())

    verbose = args.pop('verbose')
    logging.basicConfig(
        format='%(levelname)s: %(message)s',
        level=DEBUG if verbose >= 2 else INFO if verbose == 1 else WARN
    )

    try:
        expire(**args)
    except (ConnectionError, HTTPError) as e:
        print(e, file=sys.stderr)
        sys.exit(1)

    sys.exit(0)
