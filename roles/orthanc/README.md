# Role for Orthanc DICOM server in ubuntu 20.04LTS

This is the full install of the [Orthanc DICOM server](http://www.orthanc-server.com) with the webview, the DICOM rest-api and a postgres backend for the indices.
Based on Ubuntu 20.04 server edition

## Prior requirements

### System localisation and preparation on raspberry pi

#### Disable cloudinit and fix networking

```bash
sudo touch /etc/cloud/cloud-init.disabled
```

Configure networking in yaml file in `/etc/netplan/` (disable cloud init network by renaming) eg:
`/etc/netplan/50-home.yaml`:

```yaml
network:
  ethernets:
    eth0:
      dhcp4: false
      addresses:
        - 192.168.1.59/24
      gateway4: 192.168.1.254
      nameservers:
        addresses:
          - 192.168.1.254
          - 8.8.8.8
      optional: true
  version: 2
```

and bring up with `netplan apply`.

Set hostname in `/etc/hostname`.

#### System localisation

If not set otherwise then set the machine locale, timezone and keyboard appropriately.

```bash
sudo apt-get install language-pack-en
sudo locale-gen en-GB.UTF-8
sudo update-locale
sudo timedatectl set-timezone Europe/London
sudo dpkg-reconfigure keyboard-configuration
```

set hostname in `/etc/hostname`

### Access for ansible

The system should also have a working `sshd` and the ssh public key for the ansible master in `/root/.ssh/authorized_keys`.

### For configuring apt via ansible

- `apt-get install python3-apt`

### System setup for containers

If the system is an `lxd/lxc` container then:

- forwarding needs to be specified for ssh if the ansible master is on a different host than `lxd`
- if the web client or DICOM SCU is on another host (as it will be) then forwards are also needed for ports 4242 and 8042

eg on the lxd host this may be done by adding a proxy device:

```bash
sudo lxc config device add $container_name ${container_name}2242 proxy listen=tcp:0.0.0.0:2242 connect=tcp:127.0.0.1:22
sudo lxc config device add $container_name ${container_name}4242 proxy listen=tcp:0.0.0.0:4242 connect=tcp:127.0.0.1:4242
sudo lxc config device add $container_name ${container_name}8042 proxy listen=tcp:0.0.0.0:8042 connect=tcp:127.0.0.1:8042
```

or with iptables port forwarding

```bash
sudo iptables -t nat -A PREROUTING -p tcp -i eno1 --dport 2242 -j DNAT --to-destination $container_ip:22
sudo iptables -t nat -A PREROUTING -p tcp -i eno1 --dport 4242 -j DNAT --to-destination $container_ip:4242
sudo iptables -t nat -A PREROUTING -p tcp -i eno1 --dport 8042 -j DNAT --to-destination $container_ip:8042
```

In the ansible inventory `hosts` file the container has to be specified with this variant port for `ssh` eg:

```
[lxd-containers]
orthanc ansible_ssh_port=2242 ansible_ssh_host=$container_host
```

## Port configuration

Note that `setcap` is not properly supported from inside a non-root `lxc` container so we are using the default Orthanc
ports for DICOM and http rather than 104 and 80. The commented code can be reverted if being built on bare metal or a full VM.

(Looks like this was fixed in kernel 4.14 RHD20191104)

Internet access is required to the ubuntu apt repositories and [the orthanc mercurial repos](https://hg.orthanc-server.com/)

There are example users `admin` and `orthanc` in the web interface with password set in `/etc/orthanc/Configuration.yml`.
This is set in `vars/secrets.yml` which may be encrypted  with `ansible-vault` as needed.

## Security

- Orthanc runs as the non-root user `orthanc`
- Configuration file (or users section) is readable only by group `orthanc`
- Ancillary scripts embedding passwotds are readable only by group `orthanc`
- `LimitFindResults` and `LimitFindInstances` is set to 100 to prevent flooding of user interface
- `HttpsVerifyPeers` is `true`
- `OverwriteInstances` is `false`, nb this prevents update of stored instances
- `RemoteAccessAllowed` is `false` and an apache reverse proxy is used, set to use https only - a signed certificate may be required
- `WebDavEnabled` is `false` turning Webdav off altogether
- `AuthenticationEnabled` is `true` - only configured users are allowed access to web apis
- Access to post methods limited by acl in lua (currently only `admin`)
  - this may be made more comprehensive in python
- `ExecuteLuaEnabled` is `false`, disabling execution of uploaded scripts
- `DicomAlwaysAllowEcho`, `DicomAlwaysAllowStore`, `DicomAlwaysAllowFind` and `DicomAlwaysAllowGet` are all set to `false`
- `DicomCheckModalityHost` set to `true`
- Each dicom node has an explicit (extended) configuration
- Storage is not explicitly encrypted here, but if not locally hosted on a physically secure server then both the image storage area and the postgres index should be on encrypted partitions
- `DeidentifyLogs` is `true`

## Performance

- Postgres should be on an ssd partition
- `StorageCompression` is `false`
- `KeepAlive` and `TcpNoDelay` are both `true`
- There may be some performance reduction going via the https reverse-proxy.
- If scaled up then it may be worth putting the image storage on xfs/btfs


Ron Hartley-Davies May 2021

