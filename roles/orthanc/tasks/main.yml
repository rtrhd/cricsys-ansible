---
#
## Install orthanc dicom server on ubuntu 20.04/22.04 (including on Rapberry pi)
## Allows postgres or sqlite for database index but always the filesystem for dicom objects
#

# Login details on web interface (will be inserted in orthanc configuration file)
- name: Orthanc | webuser details
  include_vars: secrets.yml


- name: Orthanc | general apt package dependencies
  apt:
    name:
      - build-essential
      - pkg-config
      - aptitude
      - unzip
      - cmake
      - mercurial

- name: Orthanc | apt dependencies for dynamic build
  apt:
    name:
      - uuid-dev
      - libcurl4-openssl-dev
      - liblua5.3-dev
      - libgoogle-glog-dev
      - libgtest-dev
      - libpng-dev
      - libjpeg-dev
      - libsqlite3-dev
      - libssl-dev
      - zlib1g-dev
      - libdcmtk2-dev
      - libboost-all-dev
      - libwrap0-dev
      - libjsoncpp-dev
      - libpugixml-dev
      - libvtk7-dev
      - libgdcm-tools
      - libgdcm-dev
      - libvtkgdcm-dev
  when: not use_static_build

- name: Orthanc | apt dependencies for extended lua support
  apt:
    name:
      - "lua{{lua_version}}"
      - lua-penlight
      - lua-sql-odbc
      - lua-yaml

- name: Orthanc | apt dependencies for postgres plugin
  apt:
    name:
      - postgresql
      - postgresql-contrib # for trigraph extension
      - libpq-dev
  when: use_postgres

- name: Orthanc | apt dependencies for python plugin
  block:
    - apt:
        name:
          - libpython3-dev
          - python3-pydicom
    - apt:
        name:
          - python3-psycopg2
      when: use_postgres
  when: python_plugin

- name: Orthanc | clone repos
  hg: "name=https://hg.orthanc-server.com/{{ item.name }} revision={{ item.branch }} dest={{ build_dir }}/{{ item.name }}"
  with_items:
     - {name: orthanc, branch: "{{ orthanc_branch }}"}
     - {name: orthanc-dicomweb, branch: "{{ dicomweb_branch }}"}
     - {name: orthanc-webviewer, branch: "{{ webviewer_branch }}"}
     - {name: orthanc-databases, branch: "{{ databases_branch }}"}
     - {name: orthanc-python, branch: "{{ python_branch }}"}
     - {name: orthanc-gdcm, branch: "{{ gdcm_branch }}"}
     - {name: orthanc-stone, branch: "{{ stone_branch }}"}


# Workaround for problem with libpng config on ARM/raspberry pi 
- name: Orthanc | patch  libpng config to build missing neon on arm
  blockinfile:
    path: "{{ build_dir }}/orthanc/OrthancFramework/Resources/CMake/LibPngConfiguration.cmake"
    insertbefore: "source_group"
    block: |
        if(CMAKE_SYSTEM_PROCESSOR MATCHES "^(aarch64.*|AARCH64.*)")
          list(APPEND LIBPNG_SOURCES
            ${LIBPNG_SOURCES_DIR}/arm/arm_init.c
            ${LIBPNG_SOURCES_DIR}/arm/filter_neon_intrinsics.c
            ${LIBPNG_SOURCES_DIR}/arm/filter_neon.S
            ${LIBPNG_SOURCES_DIR}/arm/palette_neon_intrinsics.c
         )
        endif()
  when: ansible_architecture != "x86_64"

- name: Orthanc | build areas
  file: path={{ build_dir }}/{{ item }}-build state=directory mode=0755
  with_items:
    - orthanc/OrthancServer
    - orthanc-dicomweb
    - orthanc-webviewer
    - orthanc-databases/PostgreSQL
    - orthanc-python
    - orthanc-gdcm
    - orthanc-stone/Applications/StoneWebViewer/Plugin


- name: Orthanc | download wasm binaries and assets for stone webviewer
  unarchive:
    remote_src: true
    src: https://lsb.orthanc-server.com/stone-webviewer/mainline/wasm-binaries.zip
    dest: "{{ build_dir }}/orthanc-stone/"


# Build in "static" mode using downloaded library sources rather than system libs
# The HAVE_UNIX_SOCKETS amd HAVE_SYS_UN_H is a workaround for failure to build
# the unix domain socket authentication in postgres client library on raspberry pi
#
- name: debug
  debug:
    msg: "{'STATIC_BUILD': '{{static_build}}', 'ALLOW_DOWNLOADS': 'ON'}"


- name: Orthanc | build Orthanc server and default plugins with cmake
  cmake:
    source_dir: "{{ build_dir }}/orthanc/OrthancServer"
    binary_dir: "{{ build_dir }}/orthanc/OrthancServer-build"
    build_type: Release
    cache_vars:
      STATIC_BUILD: "{{static_build}}"
      ALLOW_DOWNLOADS: "ON"
      ENABLE_LUA_MODULES: "ON"


- name: Orthanc | build additional plugins with cmake
  cmake:
    source_dir: "{{ build_dir }}/{{ item }}"
    binary_dir: "{{ build_dir }}/{{ item }}-build"
    build_type: Release
    cache_vars:
      STATIC_BUILD: "{{static_build}}"
      ALLOW_DOWNLOADS: "ON"
      ORTHANC_FRAMEWORK_SOURCE: "path"
      ORTHANC_FRAMEWORK_ROOT: "{{build_dir}}/orthanc/OrthancFramework/Sources"
  with_items:
    - orthanc-dicomweb
    - orthanc-webviewer
    - orthanc-gdcm
    - orthanc-stone/Applications/StoneWebViewer/Plugin


- name: Orthanc | build the python plugin with cmake
  cmake:
    source_dir: "{{ build_dir }}/orthanc-python"
    binary_dir: "{{ build_dir }}/orthanc-python-build"
    build_type: Release
    cache_vars: 
      STATIC_BUILD: "{{static_build}}"
      PYTHON_VERSION: "{{python_versions[ansible_distribution_release]}}"
      ALLOW_DOWNLOADS: "ON"
      ORTHANC_FRAMEWORK_SOURCE: "path"
      ORTHANC_FRAMEWORK_ROOT: "{{build_dir}}/orthanc/OrthancFramework/Sources"


- name: Orthanc | build the postgres plugin with cmake (includes workaround to get unix domain socket support)
  cmake:
    source_dir: "{{ build_dir }}/orthanc-databases/PostgreSQL"
    binary_dir: "{{ build_dir }}/orthanc-databases/PostgreSQL-build"
    build_type: Release
    cache_vars:
      STATIC_BUILD: "{{static_build}}"
      HAVE_UNIX_SOCKETS: "1"
      HAVE_SYS_UN_H: "1"
      HAVE_STRUCT_SOCKADDR_UN: "1"
      ORTHANC_FRAMEWORK_SOURCE: "path"
      ORTHANC_FRAMEWORK_ROOT: "{{build_dir}}/orthanc/OrthancFramework/Sources"
  when: use_postgres


- name: Orthanc | a system group for the orthanc server
  group:
    name: "{{ orthanc_group }}"
    system: true


- name: Orthanc | a system user for the orthanc server
  user:
    name: "{{ orthanc_user }}"
    comment: "Orthanc system user"
    group: "{{ orthanc_group }}"
    home: "{{ install_dir }}"
    system: true

 
#
# NB We use the system user <-> postgres user identity to authenticate logins
# so need "local all all peer" in (eg) /etc/postgres/12/main/pg_hba.conf
# but this is the default anyway on ubuntu 20.04/22.04
#
- name: Orthanc | configure postgres
  block:
    - name: Orthanc | a postgres role for the orthanc user
      postgresql_user: name={{ orthanc_user }} role_attr_flags="LOGIN"
      become: true
      become_user: postgres

    - name: Orthanc | create the postgres database for orthanc
      postgresql_db:
        name: "{{ orthanc_db }}"
        encoding: "UTF-8"
        owner: "{{ orthanc_user }}"
      become: true
      become_user: postgres

    - name: Orthanc | add the postgres trigraph extension
      postgresql_ext:
        db: "{{ orthanc_db }}"
        name: pg_trgm
      become: true
      become_user: postgres
      notify:
        - restart postgresql
  when: use_postgres


- name: Orthanc | installation directories
  file: path={{ item }} state=directory owner={{ orthanc_user }} group={{ orthanc_group }} mode=0755
  with_items:
    - "{{ bin_dir }}"
    - "{{ plugins_dir }}"
    - "{{ aet_dir }}"
    - "{{ lua_dir }}"
    - "{{ python_dir }}"
    - "{{ storage_dir }}"
    - "{{ index_dir }}"
    - "{{ config_dir }}"
    - "{{ log_dir }}"


# The Orthanc server is a single binary 
- name: Orthanc | install binaries
  copy:
    remote_src: true
    force: true
    src: "{{ build_dir }}/orthanc/OrthancServer-build/{{ item }}"
    dest: "{{ bin_dir }}"
    mode: "0755"
  with_items:
   - Orthanc
   - OrthancRecoverCompressedFile


# Plugins are .so files in the plugins subdir, configured in the main Orthanc Configuration file
- name: Orthanc | install plugins
  copy:
    remote_src: true
    force: true
    src: "{{ build_dir }}/{{ item }}"
    dest: "{{ plugins_dir }}"
    mode: "0644"
  with_items:
    - "orthanc/OrthancServer-build/libModalityWorklists.so"
    - "orthanc/OrthancServer-build/libServeFolders.so"
    - "orthanc-dicomweb-build/libOrthancDicomWeb.so"
    - "orthanc-webviewer-build/libOrthancWebViewer.so"
    - "orthanc-gdcm-build/libOrthancGdcm.so"
    - "orthanc-stone/Applications/StoneWebViewer/Plugin-build/libStoneWebViewer.so"


- name: Orthanc | install the python plugin
  copy: remote_src=true force=true src="{{ build_dir }}/{{ item }}" dest="{{ plugins_dir }}" mode="0644"
  with_items:
    - "orthanc-python-build/libOrthancPython.so"
  when: python_plugin

- name: Orthanc | install the postgres plugin
  copy: remote_src=true force=true src="{{ build_dir }}/{{ item }}" dest="{{ plugins_dir }}" mode="0644"
  with_items:
    - "orthanc-databases/PostgreSQL-build/libOrthancPostgreSQLIndex.so"
  when: use_postgres


# Lua scripts
- name: Orthanc | install lua server-side scripts
  template:
    src: "{{ item }}.j2"
    dest: "{{ lua_dir }}/{{ item }}"
    owner: "{{ orthanc_user }}"
    group: "{{ orthanc_group }}"
    mode: "0644"
  with_items: "{{ lua_scripts }}"


# Python plugin entry point
- name: Orthanc | install python plugin script
  template: src=plugin.py.j2 dest={{ python_dir }}/plugin.py owner={{ orthanc_user }} group={{ orthanc_group }} mode="0644"
  when: python_plugin


- name: Orthanc | python javascript support script
  template: src=extend-explorer.js.j2 dest={{ python_dir }}/extend-explorer.js owner={{ orthanc_user }} group={{ orthanc_group }} mode="0644"
  when: python_plugin


# Miscellaneous command line tools/scripts
- name: Orthanc | command line scripts
  template: src="{{ item }}.j2" dest="{{ bin_dir }}/{{ item }}_{{ title_lower }}" owner={{ orthanc_user }} group={{ orthanc_group }} mode="0750"
  with_items:
    - "expire_patients"
    - "orthanc_upload"

# Set systems capablities on the Orthanc binary to let it listen on the privileged ports 80,104,443 etc
# NB Unfortunately this does not work in an unpriv container on some versions of lxd at the moment
# due to kernel limitations so in that case we may need to use the default othanc ports that are non-priv
- name: Orthanc | grant port listen rights to server
  command: "setcap 'cap_net_bind_service=+ep' {{ bin_dir }}/Orthanc"
  ignore_errors: true
  changed_when: false


#
# Users for the web interface, DICOM AETs and server plugins are all configured in this file
#
- name: Orthanc | main config file
  template:
    src: Configuration.json.j2
    dest: "{{ config_dir }}/orthanc.json"
    owner: "{{ orthanc_user }}"
    group: "{{ orthanc_group }}"
    mode: "0440"
    force: true


- name: Orthanc | subsidiary config files
  template:
    src: "config-{{item}}.json.j2"
    dest: "{{ configs_dir }}/{{ title_lower }}/{{item}}.json"
    owner: "{{ orthanc_user }}"
    group: "{{ orthanc_group }}"
    mode: "0440"
    backup: true
  with_items:
    - lua


# Template systemd service script for orthanc
- name: Orthanc | systemd service script
  template:
    src: orthanc.service.j2
    dest: "/etc/systemd/system/orthanc@.service"


# Cron job for cleaning old patients etc
- name: Orthanc | orthanc cron job
  template: src=orthanc.cron.j2 dest=/etc/cron.daily/orthanc-{{title_lower}} mode="0750" backup=true
  when: expire_patients


# Cron job for backing up postgres (all databases/aets)
- name: Orthanc | postgres cron job
  copy: src=backup-pgsql dest=/etc/cron.weekly/backup_pgsql  mode="0755" force=false
  when: use_postgres


- name: Orthanc | enable apache reverse proxy 
  block:
    - name: Orthanc | apache | apt packages
      apt:
        name:
          - apache2
          - apache2-utils
    - name: Orthanc | apache | modules
      apache2_module: name="{{item}}" state=present
      with_items:
        - ssl
        - rewrite
        - proxy
        - proxy_http
        - proxy_wstunnel
        - headers
      notify: reload apache
    - name: Orthanc | apache | include config in default HTTP virtual host
      lineinfile:
        path: /etc/apache2/sites-available/000-default.conf
        line: "     IncludeOptional conf-available/vhost80*conf"
        insertbefore: "</VirtualHost>"
      notify: reload apache
    - name: Orthanc | apache | include config in default HTTPS virtual host
      lineinfile:
        path: /etc/apache2/sites-available/default-ssl.conf
        line: "     IncludeOptional conf-available/vhost443*conf"
        insertbefore: "</VirtualHost>"
      notify: reload apache
    - name: Orthanc | apache | enable default HTTP virtual host
      command: a2ensite 000-default
      register: a2ensite_80
      changed_when: "'Enabling' in a2ensite_80.stdout"
      notify: reload apache
    - name: Orthanc | apache | enable default HTTPS virtual host
      command: a2ensite default-ssl
      register: a2ensite_443
      changed_when: "'Enabling' in a2ensite_443.stdout"
      notify: reload apache
    - name: Orthanc | apache | template install orthanc configurations
      template: src={{ item }}_orthanc.conf.j2 dest=/etc/apache2/conf-available/{{item}}_{{rproxy_path}}.conf backup=true mode="0644"
      with_items:
        - vhost443
        - vhost80
      notify: reload apache
  when: apache2_rproxy


- name: Orthanc | enable and restart service for {{title_lower}}
  systemd:
    name: "orthanc@{{title_lower}}"
    enabled: true
    state: restarted
    daemon-reload: true
