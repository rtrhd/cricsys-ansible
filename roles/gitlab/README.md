# A role for installing a local gitlab instance

The installation is straightforward as they provide a ppa and an _omnibus_ package that does all the
installation into `/opt`. It configures its own postgres and redis servers. It requires a configured email system
so it can submit emails for account managemant etc. If you specify an https url it also attempted to set up
certificates, but this relies on a valid dns A record for the declared url host, which will generally fail if for instance
we are trying to install in a container or vm.

The url and the admin (`root`) password can be declared in environment variables when the package is installed.

If installing into an `lxd` container then port forwarding  on the default `lxdba`r network can be set up from the host as follows:

```
lxc network forward create lxdbr0 10.160.155.245
lxc network forward port add lxdbr0 10.160.155.245 tcp 8099 10.122.72.45 80
```

Something similar could be done with vbox etc.

## TODO
- Set up postfix as relaying mailer to smtp.ubht.nhs.uk for everything except `*.localhost`.
- Work out how to set up https with prearranged or dummy certificates and phony dns entry
  - maybe we can drop something in /etc/hosts and get local dns resolover to pick it up
- Is there a way to add trust authentication for usernames/passwords (would need domain joining etc)
- Optionally configure https with certificates and dns A record
- See if email delivers to localhost properly
- Change email for admin user to be root@localhost or similar
- Look at kerberos auth and automatic user account gen
- Look at integration with redmine
- Try forking projects from github / bitbucket
- Have a look at systemd-resolved and /etc/hosts to see if we make a locally valid A record
- Have a look at forwarding ports on host machine

If we deploy to lxd container with an lxdbr bridge then we'll need the port forwarding, but this will
only work for http. For https we may need to deploy to a host with a _public_ ip.

