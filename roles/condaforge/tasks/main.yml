---
#
## Anaconda (Python 3) install tasks (miniconda version)
#

- name: Anaconda | package dependencies
  apt:
    name:
      # work around for issue in ansible pip module
      - python3-setuptools
      # work around for problem with pscycopg2 package
      - libpq-dev

- name: Anaconda | Check installation
  stat:
    path: "{{conda_dir}}/version-{{anaconda3_version}}"
  register: version_file
  changed_when: not version_file.stat.exists

- name: Anaconda | Installer
  block:
    - name: Anaconda | Remove old distribution
      file:
        name: "{{conda_dir}}/"
        state: absent
    - name: Anaconda | Recreate installation directory
      file:
        name: "{{conda_dir}}/"
        state: directory
    - name: Anaconda | Copy installer file into place
      get_url:
        url: "https://github.com/conda-forge/miniforge/releases/download/{{anaconda3_version}}/Miniforge3-{{anaconda3_version}}-Linux-{{ansible_architecture}}.sh" 
        dest: "/usr/local/src/"
    - name: Anaconda | Run installer
      command: "bash /usr/local/src/Miniforge3-{{anaconda3_version}}-Linux-x86_64.sh -f -b -p {{conda_dir}}"
    - name: Anaconda | Record new version
      copy:
        dest: "{{conda_dir}}/version-{{anaconda3_version}}"
        content:  "{{anaconda3_version}}"
    - name: Anaconda | Clean up installer file
      file:
        name: "/usr/local/src/Minforge3-{{anaconda3_version}}-Linux-x86_64.sh"
        state: absent
  when: version_file.changed

- name: Anaconda | Add extra packages to anaconda root environment
  conda:
    name: "{{anaconda_extras}}"
    executable: "{{conda_dir}}/bin/conda"

- name: Anaconda | Clean conda package caches
  command: "{{conda_dir}}/bin/conda clean -y --all"
  register: conda_clean
  changed_when: "'emov' in conda_clean.stdout"

- name: Anaconda | Add extra pip packages to anaconda environment
  pip:
    name: "{{anaconda_pip}}"
    executable: "{{conda_dir}}/bin/pip"
    extra_args: "--upgrade-strategy only-if-needed"

- name: Anaconda | Install anaconda switch
  template: src=use_anaconda.j2 dest=/usr/local/bin/use_anaconda mode=0755

- name: Anaconda | Desktop files
  template: src="{{item}}.j2" dest=/usr/share/applications/{{item}} mode=0644
  with_items:
    - anaconda-qtconsole.desktop
    - anaconda-spyder.desktop

- name: Anaconda | Desktop icons
  copy: src={{item}} dest=/usr/share/icons/ mode=0644
  with_items:
    - anaconda-qtconsole.png
    - anaconda-spyder.png

- name: Anaconda | Add an icon for the octave kernel
  copy:
    src: "octave-64x64.png"
    dest: "{{conda_dir}}/share/jupyter/kernels/octave/logo-64x64.png"

- name: Anaconda | Workaround for data rate limit in notebook
  lineinfile:
    name: "{{conda_dir}}/etc/jupyter/jupyter_notebook_config.py"
    line: "c.NotebookApp.iopub_data_rate_limit = 10000000"
    create: yes

# this is a bit of a mess, the installkernel command is hardwired to install to the user directory
# I suppose we could let it install to root and then copy the config to /usr/local/anaconda3/share/jupyter/kernels/
- name: Anaconda | IJulia | Check for Julia
  command: which julia
  register: have_julia
  failed_when: have_julia.rc not in [0, 1]
  changed_when: false

- name: Anaconda | IJulia | Julia version
  block:
    - name: Anaconda | IJulia | Julia version cmd
      shell: "julia --version julia --version | cut -d ' ' -f 3 | cut -d. -f1,2"
      register: julia_version
      changed_when: false
    - name: Anaconda | IJulia | Julia version fact
      set_fact:
        julia_baseversion: "{{julia_version.stdout}}"
      changed_when: false
  when: "'julia' in have_julia.stdout"

- name: Anaconda | IJulia | IJulia Kernel
  block:
   - name: Anaconda | IJulia Kernel
     command: "/usr/local/julia/bin/julia -e 'using Pkg; deleteat!(DEPOT_PATH, 1); Pkg.add(\"IJulia\"); using IJulia'"
   - name: Julia | Systemwide IJulia kernel spec
     command: "cp -r /root/.local/share/jupyter/kernels/julia-{{julia_baseversion}} {{conda_dir}}/share/jupyter/kernels/"
     args:
       creates: "{{conda_dir}}/share/jupyter/kernels/julia-{{julia_baseversion}}"
  when: "'julia' in have_julia.stdout"

# we need to remove any existing IR kernel ...
#- name: Anaconda | IRKernel | install apt-get dependencies for (system) IRkernel
#  apt:
#    state: latest
#    name:
#      - libcurl4-gnutls-dev
#
#- name: Anaconda | IRKernel | Check for Rscript
#  command: which Rscript
#  register: have_rscript
#  failed_when: have_rscript.rc not in [0, 1]
#  changed_when: false
#
#- name: Anaconda | IRKernel | install IRkernel CRAN packages dependencies via R
#  command: Rscript --slave --no-save --no-restore-history -e "if (!('{{item}}' %in% installed.packages()[,'Package'])) install.packages('{{item}}', repos='http://www.stats.bris.ac.uk/R')"
#  with_items:
#    - repr
#    - IRdisplay
#    - evaluate
#    - crayon
#    - pbdZMQ
#    - glue
#    - devtools
#    - uuid
#    - digest
#  register: rscript_result
#  changed_when: "'Installing' in rscript_result.stdout"
#  when: "'Rscript' in have_rscript.stdout"
#
#- name: IRKernel | check for IRkernel
#  command: Rscript --slave --no-save --no-restore-history -e "'IRkernel' %in% installed.packages()[,'Package']"
#  register: irkernel_install_needed
#  changed_when: "'FALSE' in irkernel_install_needed.stdout"
#  when: "'Rscript' in have_rscript.stdout"
#
#- name: IRKernel | install IRkernel from github
#  command: Rscript --slave --no-save --no-restore-history -e "if (!('IRkernel' %in% installed.packages()[,'Package'])) devtools::install_github('IRkernel/IRkernel')"
#  when: "'Rscript' in have_rscript.stdout and irkernel_install_needed.changed"
#
# odd that kernelspec is put in /usr/local/share, it really belongs to anaconda
#- name: IRKernel | install IRkernel kernel spec into jupyter (anaconda)
#  shell: . /usr/local/bin/use_anaconda; Rscript --slave --no-save --no-restore-history -e "IRkernel::installspec(user = FALSE)" creates=/usr/local/share/jupyter/kernels/ir/
#  when: "'Rscript' in have_rscript.stdout"

