#
#    /etc/profile.d/nx.sh
#
# send feedback to feedback@nomachine.com

#
#  Setting NXDIR
#
export NXDIR=/usr/NX

#
#  Expanding PATH
#
if [ -x /bin/grep ];
then
  GREPCOMMAND="/bin/grep"
else
  GREPCOMMAND="grep"
fi

if ! echo ${PATH} | $GREPCOMMAND -q $NXDIR ; then
       export PATH=${PATH}:$NXDIR/bin
fi

