#!/usr/bin/env python
#
# Nagios Plugin to test DICOM router
# R. Hartley-Davies - 05/2011-02/2018
#
# NB no validation of arguments as yet
#
from __future__ import division, print_function
import os.path, time
import sys, getopt
import string

# nagios sys exit codes
OK, WARNING, CRITICAL, UNKNOWN = range(4)

heartbeat_file = '/var/local/mridata/_heartbeat_'
max_file_age = 15 * 60 # secs

def usage():
    print("usage: check_router [-f heartbeatfile] [-c maxfileage]")

try:
    opts, args = getopt.getopt(sys.argv[1:], 'hf:c:', ['help',])
except getopt.GetoptError:
    usage()
    sys.exit(UNKNOWN)

for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(OK)
    elif o == '-f':
        heartbeat_file = a
    elif o == '-c':
        try:
            max_file_age = int(float(a) * 60)
	except ValueError:
            print('Illegal file age "%s" - must be numeric minutes' % a)
            usage() 
            sys.exit(UNKNOWN)

if args:
    print('Too many arguments')
    usage()
    sys.exit(UNKNOWN)

# first check file age
try:
    heartbeat_time = os.path.getmtime(heartbeat_file)
except OSError:
    print('Router Critical - No heartbeat: file %s missing' % heartbeat_file)
    sys.exit(CRITICAL)

# Age in seconds
heartbeat_age = time.time()-heartbeat_time
if heartbeat_age > max_file_age:
    print('Router Critical  - No heartbeat: last pulse was %d mins ago' % (heartbeat_age/60))
    sys.exit(CRITICAL)

local_status = UNKNOWN
remote_status = UNKNOWN
error_msg = ''

try:
    with open(heartbeat_file) as f:
        for l in f:
	    toks = string.split(l, ':', 3)
	    if toks[0] == 'LOCAL':
		if toks[1] == 'OK':
                    local_status = OK
		else:
		    local_status = CRITICAL
		    error_msg += toks[2]
	    elif toks[0] == 'REMOTE':
		if toks[1] == 'OK':
                    remote_status = OK
		else:
		    remote_status = CRITICAL
		    error_msg += toks[2]
except IOError:
    print('Router Critical: no heartbeat: file %s unreadable' % heartbeat_file)
    sys.exit(CRITICAL)


if local_status == OK and remote_status == OK:
    print('Router OK - Alive and Well')
    sys.exit(OK)
elif local_status == CRITICAL and remote_status == CRITICAL:
    print('Router Critical - No response either end')
    sys.exit(CRITICAL)
elif local_status == UNKNOWN or remote_status == UNKNOWN:
    print('Router Unknown - Problem reading heartbeat file')
    sys.exit(UNKNOWN)
elif local_status == CRITICAL and remote_status == OK:
    print('Router Critical - Relay dicom server not responding: %s' % error_msg)
    sys.exit(CRITICAL)
elif local_status == OK and remote_status == CRITICAL:
    print('Router Warning - University dicom server not responding: %s' % error_msg)
    sys.exit(WARNING)
else:
    print('Router Unknown - Internal error in plugin')
    sys.exit(UNKNOWN)
