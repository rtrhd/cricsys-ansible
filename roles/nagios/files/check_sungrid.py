#!/usr/bin/python

'''
check_sungrid: plugin for nagios to check the SGE status of a host and
               its queues using qhost. If no host specified then checks
               status of all configured hosts.
'''
from __future__ import division, print_function
import os
import subprocess

__version__ = '0.2'

os.environ['SGE_ROOT'] = '/opt/sge'
QHOST = '/opt/sge/bin/lx-amd64/qhost'

OK, WARNING, CRITICAL, UNKNOWN = range(4)

child_pid = None

def handle_alarm(signum, frame):
    ''' Handler for time outs '''
    try:
        if child_pid != 0:
            os.kill(child_pid, signal.SIGKILL)
    except OSError:
        pass
    print('Execution timeout exceeded')
    sys.exit(UNKNOWN)
    

def check_environment():
    ''' Check the qhost command can be run
          returns list of (errorcode, message) tuples
    '''
    if not os.access(QHOST, os.X_OK):
        return [(UNKNOWN, 'Cannot access %s' % QHOST)]

    with open(os.devnull, 'w') as tempf:        
        if subprocess.call([QHOST, '-help'], stdout=tempf) != 0:
            return [(UNKNOWN, 'Cannot execute %s' % QHOST)]
    return []
    

def check_host(hostname, warning_states, critical_states):
    ''' Runs the qhost command for the specified host
          returns list of  (errorcode, message) tuples
    '''
    try:
        lines = subprocess.check_output([QHOST, '-h', hostname, '-q']).split('\n')
        
        # Index of host record (if unknown this will fail with index error)
        try:
            index = [i for i, line in enumerate(lines) if line.startswith(hostname)][0]
        except IndexError:
            return [(CRITICAL, 'Host unknown %s' % hostname)]

        # The host record
        hline = lines[index]
        fields = hline.split()

        _, arch, ncpu, nsoc, ncor, nhtr, load, memtot, memuse, swapto, swapus = fields
        if any(field == '-' for field in fields):
            return [(CRITICAL, 'Execd not responding on %s' % hostname)]

        # The queue records
        qlines = lines[index+1:]
        warnings = []
        critical = []
        for qline in qlines:
            fields = qline.split()
            # no news is good news ..
            if len(fields) < 4:
                continue               
            states = fields[3]
            if any(state in warning_states for state in states):
                warnings.append('%s(%s)' % (fields[0], fields[3]))
            if any(state in critical_states for state in states):
                critical.append('%s(%s)' % (fields[0], fields[3]))            

        return [(WARNING, w) for w in warnings] + [(CRITICAL, c) for c in critical]
                    
    except subprocess.CalledProcessError as e:
        return [(UNKNOWN, 'Problem executing %s' % QHOST)]

    return []


def check_all_hosts(warning_states, critical_states):
    ''' Runs the qhost command locally for all hosts
          returns a list of (errorcode, message) tuples or []
    '''
    errors = []
    try:
        lines = subprocess.check_output([QHOST, '-q']).split('\n')
        lines = [line for line in lines if line[:6] not in ['HOSTNA', '------', 'global']]
        hostindices = [i for i, line in enumerate(lines) if line[:2].isalpha()]
        # now need to chop up on the indices
        for i,j in zip(hostindices, hostindices[1:] + [len(lines)]):
            hline = lines[i]
            fields = hline.split()
            hostname, arch, ncpu, nsoc, ncor, nhtr, load, memtot, memuse, swapto, swapus = fields
            if any(field == '-' for field in fields):
                errors.append((CRITICAL, '%s: field unavailable' % hostname))            
            
            # The queue records
            qlines = lines[i+1:j]
            warnings = []
            critical = []
            for qline in qlines:
                fields = qline.split()
                # no news is good news ..
                if len(fields) < 4:
                    continue               
                states = fields[3]
                if any(state in warning_states for state in states):
                    warnings.append('%s: %s(%s)' % (hostname, fields[0], fields[3]))
                if any(state in critical_states for state in states):
                    critical.append('%s: %s(%s)' % (hostname, fields[0], fields[3]))
                             
            errors += [(WARNING, w) for w in warnings] + [(CRITICAL, c) for c in critical]
    except subprocess.CalledProcessError as e:
        return [(UNKNOWN, 'Problem executing %s' % QHOST)]

    return errors


def host_stats(hostname):
    '''Return statistics for specified node as a dictionary.
    '''
    lines = subprocess.check_output([QHOST, '-h', hostname]).split('\n')
    
    # Index of host record (if unknown this will fail with index error)
    try:
        index = [i for i, line in enumerate(lines) if line.startswith(hostname)][0]
    except IndexError:
        return [(CRITICAL, 'Host unknown %s' % hostname)]

    # The host record
    hline = lines[index]
    fields = hline.split()

    hostname, arch, ncpu, nsoc, ncor, nhtr, load, memtot, memuse, swapto, swapus = fields
    stats = {
        'hostname': hostname,
        'arch':     arch,
        'nscpu':    ncpu,
        'ncor':     ncor,
        'nhtr':     nhtr,
        'load':     load,
        'memtot':   memtot,
        'memuse':   memuse,
        'swapto':   swapto,
        'swapus':   swapus
    }
    for stat in stats:
        if stat in ['nscpu', 'ncor', 'nhtr']:
            try:
                stats[stat] = int(stats[stat])
            except ValueError:
                pass
    return stats


def total_stats():
    '''Return statistics for all nodes. A list of host statistic dicts.
    '''
    stats = []

    lines = subprocess.check_output([QHOST]).split('\n')
    lines = [line for line in lines if line[:6] not in ['HOSTNA', '------', 'global']]
    hostlines = [line for line in lines if line[:2].isalpha()]

    for hline in hostlines:
        fields = hline.split()
        hostname, arch, ncpu, nsoc, ncor, nhtr, load, memtot, memuse, swapto, swapus = fields
        hoststats = {
            'hostname': hostname,
            'arch':     arch,
            'nscpu':    ncpu,
            'ncor':     ncor,
            'nhtr':     nhtr,
            'load':     load,
            'memtot':   memtot,
            'memuse':   memuse,
            'swapto':   swapto,
            'swapus':   swapus
        }
        for hstat in hoststats:
            if hstat in ['nscpu', 'ncor', 'nhtr']:
                try:
                    hoststats[hstat] = int(hoststats[hstat])
                except ValueError:
                    pass
        stats.append(hoststats)

    return stats           


if __name__ == '__main__':
    import argparse
    import sys
    import signal
 
    parser = argparse.ArgumentParser()
    parser.add_argument('-V', '--version',  help='Show version number', action='store_true')
    parser.add_argument('-H', '--hostname', help='The hostname of the node to check', default=None)
    parser.add_argument('-w', '--warning',  help='The queue states that will cause a warning. (eg -w ds)', default='ds')
    parser.add_argument('-c', '--critical', help='The queue states that will cause a critical error.  (eg -c au)', default='au')
    parser.add_argument('-v', '--verbose',  help='Verbosity', action='store_true')
    parser.add_argument('-t', '--timeout',  help='Plugin timeout in seconds [%default]', type=int, default=15)
    
    args = parser.parse_args()
    if args.version:
        print('%s %s' % (sys.argv[0], __version__))
        sys.exit(0)
 
    signal.signal(signal.SIGALRM, handle_alarm)
    signal.alarm(args.timeout)

    errors= []
    try:
        errors += check_environment()
        if args.hostname:
            errors += check_host(args.hostname, args.warning, args.critical)
        else:
            errors += check_all_hosts(args.warning, args.critical)    
            
        if errors:
            if any(err[0] == UNKNOWN for err in errors):
                print('UNKNOWN: %s' % [err[1] for err in errors])
                sys.exit(UNKNOWN)
            elif any(err[0] == CRITICAL for err in errors):
                print('CRITICAL: %s' % [err[1] for err in errors])
                sys.exit(CRITICAL)
            else:
                print('WARNING: %s' % [err[1] for err in errors])
                sys.exit(WARNING)
        else:
            if args.hostname:
                hstats = host_stats(args.hostname)
                ncores = hstats['ncor']
                print('OK: SGE node "%s" alive and well - [%d cores]' % (args.hostname, ncores))
            else:
                stats = total_stats()
                ncores = sum(hstats['ncor'] for hstats in stats)
                print('OK: Grid alive and well - [%d cores]' % ncores)
            sys.exit(OK)
    except OSError as e:
        print(e)
        sys.exit(UNKNOWN)

