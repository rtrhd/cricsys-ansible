#!/usr/bin/python
from __future__ import division, print_function
import os
import subprocess
import tempfile
import re

__version__ = '0.3.1'

# Add this to your "/etc/sudoers" or "/etc/sudoers.d/nagios" file:
# nagios ALL=(root) NOPASSWD: /sbin/iptables -L *
# nagios ALL=(root) NOPASSWD: /sbin/iptables-save *

# TODO: special rules for single machine firewall (canopus) or routers (route-to-it)

# Nagios states
OK, WARNING, CRITICAL, UNKNOWN = range(4)
class NagiosUnknown(UserWarning):
    pass
class NagiosCritical(UserWarning):
    pass
class NagiosWarning(UserWarning):
    pass
    
SUDO       = '/usr/bin/sudo'
IPTABLES   = '/sbin/iptables'
IPTABSAVE  = '/sbin/iptables-save'

SAVEDSTATE = '/etc/iptables/rules.v4'


RULES = {
    'filter': [
     ],
    'nat': [
        '-A PREROUTING -P  tcp -m tcp --dport 104 -j REDIRECT --to-ports 11112'        
     ]
}


def number_of_rules(chain):
    ''' Returns the number of rules for a given chain or zero'''
    rules = subprocess.check_output([SUDO, IPTABLES, '-L', chain, '-n', '--line-numbers']).split('\n')
    try:
        return int(rules[-1].split(' ')[0])
    except ValueError:
        return 0


def policy(chain):
    ''' Returns the default policy for the given chain'''
    lines = subprocess.check_output([SUDO, IPTABLES, '-L', chain, '-n']).split('\n')
    lines = [line for line in lines if 'policy' in line]
    return lines[0].split(' ')[3][:-1]


def sanitize(line):
    '''Remove redundant white space and fix case to make string matching more robust'''
    return ' '.join(line.strip().upper().split())


def check_command():
    ''' Check that iptables is available and functioning'''
    if not os.path.isfile(IPTABLES):
        raise NagiosUnknown('Command iptables not found')

    with open(os.devnull, 'w') as tempf:        
        if subprocess.call([IPTABLES, '-V'], stdout=tempf) != 0:
            raise NagiosUnknown('Cannot run iptables')

    with open(os.devnull, 'w') as tempf:
        if subprocess.call([SUDO, IPTABLES, '-L', '-n'], stderr=subprocess.STDOUT, stdout=tempf) != 0:
            raise NagiosCritical('Cannot run iptables with sudo - check required nagios entries in sudoers')
    return


def check_chains():
    ''' Check that the chains have rules installed'''
    for chain in ['INPUT', 'FORWARD', 'OUTPUT']:
        if number_of_rules(chain)<1 and policy(chain) != 'DROP':
            raise NagiosCritical('%s chain has no rules and default policy is not DROP' % chain)
    return


def check_fixed_rules():
    ' Check lists of fixed rules which must be present'
    missing_rules = []
    for table in RULES:
        lines = subprocess.check_output([SUDO, IPTABSAVE, '--table', table]).split('\n')   
        lines = [sanitize(line) for line in lines if line.startswith('-A')] 
        for entry in RULES[table]:
            if sanitize(entry) not in lines:
                missing_rules.append(entry)
    if missing_rules:
        raise NagiosCritical('Missing rules %s' % missing_rules)           
    return
            
                    
def check_saved_state():
    ''' Check the current and save states are consistent, return the number of rules'''
    if not os.path.isfile(SAVEDSTATE):
        raise NagiosWarning('No saved state found')
 
    # regex to remove counters if they were saved
    p = re.compile(r'^\[\d{1,25}:\d{1,25}\]\s*')

    try:
        # just match the '-A' records as other parts may not be stable
        with open(SAVEDSTATE) as file:   
            linesa = file.readlines()
            linesa = [p.sub('', line) for line in linesa]
            linesa = [sanitize(line) for line in linesa if line.startswith('-A')]
        
        linesb = subprocess.check_output([SUDO, IPTABSAVE]).split('\n')   
        linesb = [sanitize(line) for line in linesb if line.startswith('-A')] 

        if set(linesa) != set(linesb):
            raise NagiosWarning('Current and saved rule sets differ: %s' % set(linesa).symmetric_difference(set(linesb)))
        return len(set(linesa))
    except EnvironmentError as e:
            raise NagiosUnknown('Unable to process saved rules: %s', e)
        
        
if __name__ == "__main__":
    import argparse
    import sys
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', help='show version number', action='store_true')
    args = parser.parse_args()
    if args.version:
        print('check_iptables %s' % __version__)
        sys.exit(0)

    try:
        check_command()
        check_chains()
        check_fixed_rules()
        nrules = check_saved_state() 
        print('OK: iptables is running (%d rules)' % nrules)
        sys.exit(OK)
    except NagiosUnknown as e:
        print('Unknown: %s' % e)
        sys.exit(UNKNOWN)
    except NagiosWarning as e:
        print('Warning: %s' % e)
        sys.exit(WARNING)
    except NagiosCritical as e:
        print('Critical: %s' % e)
        sys.exit(CRITICAL)     

