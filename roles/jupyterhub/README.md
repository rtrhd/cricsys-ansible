# Role for JupyterHub on Ubuntu

## Outstanding Issues

- Can't connect to Kernel when using proxy - DONE
  - works fine when going direct so must be to do with rewrite rules for web sockets
  - OK inside VirtualHost it seems to work
- Needs firewall opened for 80/443 - DONE
- Paths in proxy and jupyterlab config have to match - DONE
- Proxy doesn't seem to work with trailing '/' - DONE
- Proxy doesn't work inside 443 VirtualHost - DONE
  - won't seem to work inside any virtualhost
  - Seem to need just a single VirtualHost defineition so remove default ones
- Make sure db area exists - DONE
- Install hub extension in lab - DONE
  - problem with version of nodejs in anaconda
  - fixed by updating nodejs explicitly 
- Patch html path for lab - DONE
- Hub extension in lab not working - DONE
  - OK, fixed now path is html path is set
- System V Init script fails without details - DONE
  - tried systemd instead
    - systemd starts and runs but fails to spawn user sessions
    - seem to have processes owned by jhub ..
    - not sure where errors are going
    - problem with path etc?
    - tweaked PATH and Group
    - error state on stopping as prog returns code 143
    - fixed by adding SuccessExitStatus=143
    - all seems OK

- Need UOB certificate for server rather than self signed snakeoil
- Certificate dialog broken on firefox
