function Easy_volume

% This function computes the total and partial volumes of grey or white matter
% 1st select data, i.e. segemented, normalized and modulated GM or WM
% 2nd select masks, i.e. anatomical/functional areas (needs to be already coregistered)
% 3rd select the saving directory
%
% OUTPUTS
% ---- if only total volumes are computed
%      matlab file called total [grey matter/white matter/csf] volumes
%      once loaded results are in a variable called results such as
%      results{1} returns the files that were selected in the same order
%      results{2} returns the corresponding volumes in litres
%
% ---- if partial volumes are also computed
%      matlab file called results_[grey_matter/white_matter/csf] 
%      once loaded results are in a variable called results such as
%      results.subjects_names returns the files that were selected in the same order
%      results.total_[grey_matter/white_matter/csf]_volume returns the corresponding volumes in litres
%      results.partial_grey_matter_volume{1} is for the 1st region analyzed with
%      results.partial_grey_matter_volume{1}.roi_name = the name of the 1st mask
%      results.partial_grey_matter_volume{1}.volumes = the volumes in the roi following subject's name
%
% thanks to J. Ashburner who got the computation -- just made stuffs around
% Cyril Pernet -v4 -20/Jan/2010 -tested on SPM8

clc
clear all
spm_defaults
disp('if you feel like acknowledging the use of this script you could say something like')
disp('Total and partial volumes were computed using masks created using (cite your mask source)')
disp('and following the procedure described in Pernet et al (Pernet et al. (2009). HBM, 30, 2278-2292')
disp(' ')
disp('if not Niftii, be aware of the orientation, spm_defaults are used')
disp(' ')
warning off

%% get the data

P1   = spm_select(Inf,'image','Select modulated images');

if isempty(P1) == 1
    return
end

V1   = spm_vol(deblank(P1));
Volume_total = zeros(size(P1,1),1);
disp('compute total volumes');
disp(' ')

for j=1:size(P1,1),
    vol1 = 0;
    for i=1:V1(j).dim(3)
        img1 = spm_slice_vol(V1(j),spm_matrix([0 0 i]),V1(j).dim(1:2),0);
        try
            img1 = img1(finite(img1));
        catch
            img1 = img1(isfinite(img1));
        end
        vol1 = vol1 + sum(img1(:));
    end;
    voxvol1 = abs(det(V1(j).mat(1:3,1:3)))*1e-6;
    fprintf('%g voxels, %g litres\n', vol1, vol1*voxvol1);
    Volume_total(j) = vol1*voxvol1;
end


%% stop or continue

button = questdlg('continue and compute partial volumes','More volume compuation','Yes','No','Yes');
if strcmp(button,'No')
    
    % save total volume info
    name = questdlg('were the images','image type','grey matter','white matter','CSF','grey matter');
    name = sprintf('total %s volumes',name);
    % where to save
    save_dir = uigetdir('save directory');
    if save_dir == 0
        return
    end
    cd (save_dir);
    % save
    results = {P1, Volume_total};
    save ([name], 'results');
    return
end


%% get the masks

P2   = spm_select(Inf,'image','Select coregistered mask images');
if isempty(P2) == 1
    return
end

V2   = spm_vol(P2);

%% make masked images and compute their volume

% simply multiply the data by the mask
% use spm_imcal to multiply and write the new image
Outputdir = uigetdir('Select output directory');
cd(Outputdir);
disp('compute partial volumes');
disp(' ')

% some stuffs are fixed
hold=4; type=4; mask=0; dmtx=0;
args = {{dmtx,mask,hold}};
dt  = [type spm_platform('bigend')];
Volumes_mask = zeros(size(P1,1),size(P2,1));

% i1 is the mask and i2 is the brain
% i1 is > 0 to make sure this a binary image, i.e. this will jot change the
% counting of the volume as we .* by 1
f = '(i1>0).*i2';

for mask = 1:size(P2,1) % for each mask
    fprintf('compute for %s', P2(mask,:));
    disp(' ')
    
    for i = 1:size(P1,1) % for each segmented image
        
        % get a name for the new images
        name = sprintf('%s/img%g_mask%g.img', Outputdir,i,mask);
        
        % get Vi
        V3(1) = V2(mask) ;
        V3(2) = V1(i) ;
        
        % check dimensions
        if sum(V3(1).dim == V3(2).dim) ~= 3
            disp('image dimensions are different');
            disp('please coregister the mask image(s)')
            return
        end
        
        % get Vo
        Vo = struct(	'fname',	name,...
            'dim',		V1(i).dim(1:3),...
            'dt',		dt,...
            'mat',		V1(i).mat,...
            'descrip',	'spm - algebra');
        
        Vo   = spm_imcalc(V3,Vo,f,args{:});
        
        % compute the volume
        vol3 = 0;
        for j=1:Vo.dim(3),
            img3 = spm_slice_vol(Vo,spm_matrix([0 0 j]),Vo.dim(1:2),0);
            try
                img3 = img3(finite(img3));
            catch
                img3 = img3(isfinite(img3));
            end
            vol3 = vol3 + sum(img3(:));
        end;
        voxvol3 = abs(det(Vo.mat(1:3,1:3)))*1e-6;
        fprintf('%g voxels, %g litres\n', vol3, vol3*voxvol3);
        Volumes_mask(i,mask) = vol3*voxvol3;
    end
    disp(' ');
end

%% save volume into a txt file

name = questdlg('were the images','image type','grey matter','white matter','CSF','grey matter');
    
results.subjects_name = P1;
if strcmp(name,'grey matter')
    
    results.total_grey_matter_volume = Volume_total;
    for mask = 1:size(P2,1)
        results.partial_grey_matter_volume{mask}.roi_name = P2(mask,:);
        results.partial_grey_matter_volume{mask}.volumes = Volumes_mask(:,mask);
    end
    save results_grey_matter results
    
elseif strcmp(name,'white matter')
    
    results.total_white_matter_volume = Volume_total;
    for mask = 1:size(P2,1)
        results.partial_white_matter_volume{mask}.roi_name = P2(mask,:);
        results.partial_white_matter_volume{mask}.volumes = Volumes_mask(:,mask);
    end
    save results_white_matter results
    
else
    
    results.total_csf_volume = Volume_total;
    for mask = 1:size(P2,1)
        results.partial_csf_volume{mask}.roi_name = P2(mask,:);
        results.partial_csf_volume{mask}.volumes = Volumes_mask(:,mask);
    end
    save results_csf results
    
end




