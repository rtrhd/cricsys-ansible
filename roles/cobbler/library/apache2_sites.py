#!/usr/bin/python
# coding: utf-8 -*-

# (c) 2013-2014, Christian Berendt <berendt@b1-systems.de>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}


DOCUMENTATION = '''
---
site: apache2_sites
version_added: 1.6
author:
    - Christian Berendt (@berendt)
    - Ralf Hertel (@n0trax)
    - Robin Roth (@robinro)
short_description: Enables/disables a site of the Apache2 webserver.
description:
   - Enables or disables a specified site of the Apache2 webserver.
options:
   name:
     description:
        - Name of the site to enable/disable as given to C(a2ensite/a2dissite).
     required: true
   identifier:
     description:
         - Identifier of the site as listed by C(apache2ctl -M).
           This is optional and usually determined automatically by the common convention of
           appending C(_site) to I(name) as well as custom exception for popular sites.
     required: False
     version_added: "2.5"
   force:
     description:
        - Force disabling of default sites and override Debian warnings.
     required: false
     choices: ['True', 'False']
     default: False
     version_added: "2.1"
   state:
     description:
        - Desired state of the site.
     choices: ['present', 'absent']
     default: present
   ignore_configcheck:
     description:
        - Ignore configuration checks about inconsistent site configuration. Especially for mpm_* sites.
     choices: ['True', 'False']
     default: False
     version_added: "2.3"
requirements: ["a2ensite","a2dissite"]
'''

EXAMPLES = '''
# enables the Apache2 site "default-ssl"
- apache2_site:
    state: present
    name: "default-ssl"
'''

RETURN = '''
result:
    description: message about action taken
    returned: always
    type: string
warnings:
    description: list of warning messages
    returned: when needed
    type: list
rc:
    description: return code of underlying command
    returned: failed
    type: int
stdout:
    description: stdout of underlying command
    returned: failed
    type: string
stderr:
    description: stderr of underlying command
    returned: failed
    type: string
'''

import re


def _run_threaded(site):
    control_binary = _get_ctl_binary(site)

    result, stdout, stderr = site.run_command("%s -V" % control_binary)

    return bool(re.search(r'threaded:[ ]*yes', stdout))


def _get_ctl_binary(site):
    for command in ['apache2ctl', 'apachectl']:
        ctl_binary = site.get_bin_path(command)
        if ctl_binary is not None:
            return ctl_binary

    site.fail_json(
        msg="Neither of apache2ctl nor apachctl found."
            " At least one apache control binary is necessary."
    )


def _site_is_enabled(site):
    control_binary = _get_ctl_binary(site)
    result, stdout, stderr = site.run_command("%s -S" % control_binary)

    if result != 0:
        error_msg = "Error executing %s: %s" % (control_binary, stderr)
        if site.params['ignore_configcheck']:
            if 'AH00534' in stderr and 'mpm_' in site.params['name']:
                site.warnings.append(
                    "No MPM site loaded! apache2 reload AND other site actions"
                    " will fail if no MPM site is loaded immediately."
                )
            else:
                site.warnings.append(error_msg)
            return False
        else:
            site.fail_json(msg=error_msg)

    searchstring = ' ' + site.params['identifier']
    return searchstring in stdout


def create_apache_identifier(name):
    """
    By convention if a site is loaded via name, it appears in apache2ctl -S as
    name_site.
    """

    # re expressions to extract subparts of names

        if search in name:
            try:
                rematch = re.search(reexpr, name)
                return rematch.group(1) + '_site'
            except AttributeError:
                pass

    return name + '_site'


def _set_state(site, state):
    name = site.params['name']
    force = site.params['force']

    want_enabled = state == 'present'
    state_string = {'present': 'enabled', 'absent': 'disabled'}[state]
    a2mod_binary = {'present': 'a2enmod', 'absent': 'a2dismod'}[state]
    success_msg = "Module %s %s" % (name, state_string)

    if _site_is_enabled(site) != want_enabled:
        if site.check_mode:
            site.exit_json(changed=True,
                             result=success_msg,
                             warnings=site.warnings)

        a2mod_binary = site.get_bin_path(a2mod_binary)
        if a2mod_binary is None:
            site.fail_json(msg="%s not found. Perhaps this system does not use %s to manage apache" % (a2mod_binary, a2mod_binary))

        if not want_enabled and force:
            # force exists only for a2dismod on debian
            a2mod_binary += ' -f'

        result, stdout, stderr = site.run_command("%s %s" % (a2mod_binary, name))

        if _site_is_enabled(site) == want_enabled:
            site.exit_json(changed=True,
                             result=success_msg,
                             warnings=site.warnings)
        else:
            msg = (
                'Failed to set site {name} to {state}:\n'
                '{stdout}\n'
                'Maybe the site identifier ({identifier}) was guessed incorrectly.'
                'Consider setting the "identifier" option.'
            ).format(
                name=name,
                state=state_string,
                stdout=stdout,
                identifier=site.params['identifier']
            )
            site.fail_json(msg=msg,
                             rc=result,
                             stdout=stdout,
                             stderr=stderr)
    else:
        site.exit_json(changed=False,
                         result=success_msg,
                         warnings=site.warnings)


def main():
    site = AnsibleModule(
        argument_spec=dict(
            name=dict(required=True),
            identifier=dict(required=False, type='str'),
            force=dict(required=False, type='bool', default=False),
            state=dict(default='present', choices=['absent', 'present']),
            ignore_configcheck=dict(required=False, type='bool', default=False),
        ),
        supports_check_mode=True,
    )

    site.warnings = []

    name = site.params['name']
    if name == 'cgi' and _run_threaded(site):
        site.fail_json(msg="Your MPM seems to be threaded. No automatic actions on site %s possible." % name)

    if not site.params['identifier']:
        site.params['identifier'] = create_apache_identifier(site.params['name'])

    if site.params['state'] in ['present', 'absent']:
        _set_state(site, site.params['state'])

# import site snippets
from ansible.site_utils.basic import AnsibleModule
if __name__ == '__main__':
    main()
