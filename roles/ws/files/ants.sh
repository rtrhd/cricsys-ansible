# bash rc file for ANTs (RHD April-2015)
ANTS=/usr/local/ANTs
PATH=${PATH}:${ANTS}/bin
# Required by shell scripts 
# NB Trailing separator due to path construction bugs
ANTSPATH=$ANTS/bin/
export PATH ANTSPATH
