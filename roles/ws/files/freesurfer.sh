#
# Simplified shell start up script for freesurfer (avoids bashisms of standard one)
# RHD june 2015
#

FREESURFER_HOME=/usr/local/freesurfer
export FREESURFER_HOME

SUBJECTS_DIR=$FREESURFER_HOME/subjects
export SUBJECTS_DIR

FUNCTIONALS_DIR=$FREESURFER_HOME/sessions
export FUNCTIONALS_DIR

# assumes paths are set up for matlab as well (we'll do this separately in ansible)
FSFAST_HOME=$FREESURFER_HOME/fsfast
export FSFAST_HOME

LOCAL_DIR=$FREESURFER_HOME/local
export LOCAL_DIR

MNI_DIR=$FREESURFER_HOME/mni
export MNI_DIR

MINC_BIN_DIR=$FREESURFER_HOME/mni/bin
export MINC_BIN_DIR
MINC_LIB_DIR=$MNI_DIR/lib
export MINC_LIB_DIR
MNI_DATAPATH=$MNI_DIR/data
export MNI_DATAPATH
MNI_PERL5LIB=$MINC_LIB_DIR/perl5/5.8.5
export MNI_PERL5LIB
PERL5LIB=$MNI_PERL5LIB:$MNI_DIR/lib64/perl5
export PERL5LIB

FMRI_ANALYSIS_DIR=$FSFAST_HOME
export FMRI_ANALYSIS_DIR

if [ -n "$FSLDIR" ]; then
    FSL_DIR=$FSLDIR
elif [ -e /usr/local/fsl ]; then
    FSL_DIR=/usr/local/fsl
fi
export FSL_DIR

if [ -z "$FSF_OUTPUT_FORMAT" ]; then
    FSF_OUTPUT_FORMAT=nii.gz
fi
export  FSF_OUTPUT_FORMAT

# Local ImageMagick stuff
if [ -e /usr/bin/display ]; then
    FSLDISPLAY=/usr/bin/display
    export FSLDISPLAY
fi
if [ -e /usr/bin/convert ]; then
    FSLCONVERT=/usr/bin/convert
    export FSLCONVERT
fi

PATH=$FREESURFER_HOME/bin:$FREESURFER_HOME/tktools:$FSFAST_HOME/bin:$MINC_BIN_DIR:$PATH
export PATH

# Turns on "fixing" of group surface area.
FIX_VERTEX_AREA=
export FIX_VERTEX_AREA

OS=`uname -s`
export OS

