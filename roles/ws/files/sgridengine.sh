#
# Cut down bash (only) profile.d entry for sun grid engine (RHD 21/02/2012)
#
export SGE_ROOT=/opt/sge
if [ -d $SGE_ROOT -a -x $SGE_ROOT/util/arch ]; then
    export SGE_ARCH=`$SGE_ROOT/util/arch`
    export PATH=$PATH:$SGE_ROOT/bin:$SGE_ROOT/bin/$SGE_ARCH

    export SGE_CELL=default
    export SGE_CLUSTER_NAME=cricgrid
    export SGE_QMASTER_PORT=6444
    export SGE_EXECD_PORT=6445

    if [ "$MANPATH" = "" ]; then
        MANPATH=`$SGE_ROOT/util/arch -m`
    fi
    export MANPATH=$SGE_ROOT/man:$MANPATH
else
    unset SGE_ROOT
fi
