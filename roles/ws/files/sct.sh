# They were setting the `PYTHONPATH` from here by sourcing `$SCT_BIN/bin/sct_env` but all the bash scripts
# in `$SCT_BIN/bin` are vectored through `sct_launcher` (nb they are copies rather than symlinks)
# which sets the env so we can avoid setting anything here that may mess up the python environment.
# `sct_launcher` seems to call corresponding python files in `$SCT_BIN/scripts` as top level scripts
# rather than using the standard method of packages and entry points
# RHD 20180618

SCT_DIR=/usr/local/sct
PATH=$PATH:$SCT_DIR/bin
export SCT_DIR PATH
